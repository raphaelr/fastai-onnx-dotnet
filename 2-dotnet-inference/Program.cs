﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using SkiaSharp;
using Microsoft.ML.OnnxRuntime;
using Microsoft.ML.OnnxRuntime.Tensors;

namespace PetBreedClassifier {
    class Program {
        static int Main(string[] args) {
            if (args.Length < 1 || !File.Exists(args[0])) {
                Console.WriteLine("Usage: ./PetBreedClassifier path-to-image.jpg");
                return 1;
            }

            // Load the image from disk
            using var originalImage = SKImage.FromEncodedData(args[0]);
            
            // Resize it to 224x244 pixels
            const int desiredWidth = 224, desiredHeight = 224;
            var imageInfo = new SKImageInfo(desiredWidth, desiredHeight, SKColorType.Rgba8888);

            using var resizedSurface = SKSurface.Create(imageInfo);
            using var paint = new SKPaint { FilterQuality = SKFilterQuality.High };
            resizedSurface.Canvas.DrawImage(originalImage, imageInfo.Rect, paint);
            
            // Get the raw pixel data
            var bytes = new byte[imageInfo.BytesSize];
            var pixelBuffer = IntPtr.Zero;
            try {
                pixelBuffer = Marshal.AllocHGlobal(imageInfo.BytesSize);
                resizedSurface.ReadPixels(imageInfo, pixelBuffer, imageInfo.RowBytes, 0, 0);
                Marshal.Copy(pixelBuffer, bytes, 0, imageInfo.BytesSize);
            } finally {
                Marshal.FreeHGlobal(pixelBuffer);
            }
            
            // Transform the pixel data
            var floats = new float[3 * desiredWidth * desiredHeight];
            
            // Loop over every pixel
            for (var y = 0; y < desiredHeight; y++) {
                for (var x = 0; x < desiredWidth; x++) {
                    for (var channel = 0; channel < 3; channel++) {
                        var destIndex = channel * desiredHeight * desiredWidth + y * desiredWidth + x;
                        var sourceIndex = y * imageInfo.RowBytes + x * imageInfo.BytesPerPixel + channel;
                        floats[destIndex] = bytes[sourceIndex] / 255.0f;
                    }
                }
            }
            
            // Running the model
            // Note: Unlike as in the blog post, I've set up the project to copy the model and vocab file
            // to the build directory. This makes the program slightly more usable, since it will work no matter
            // from which directory you start it.
            var modelDirectory = AppContext.BaseDirectory;
            using var model = new InferenceSession(Path.Combine(modelDirectory, "pet-breed.onnx"));
            var classNames = File.ReadAllLines(Path.Combine(modelDirectory, "pet-breed.vocab.txt"));
            
            // Create the input tensor. This has to be the exact same shape
            // you specified during torch.onnx.export, i.e. (1, 3, 224, 224)
            var imageAsTensor = floats.ToTensor()
                .Reshape(new[] { 1, 3, desiredWidth, desiredHeight });

            var modelInputs = new[] {
                // Use the same name you specified during torch.onnx.export, i.e. "image"
                NamedOnnxValue.CreateFromTensor("image", imageAsTensor),
            };

            // Action!
            var modelOutputs = model.Run(modelInputs);
            var activations = modelOutputs.Single().AsTensor<float>();
            
            // Output the softmax probability
            // Pass 1: Compute activations.exp().sum()
            var expSum = activations.Select(x => Math.Exp(x)).Sum();

            // Pass 2: As the loop in the previous code snippet, but with probabilities:
            for (var i = 0; i < activations.Length; i++) {
                float activation = activations.GetValue(i);
                var probability = Math.Exp(activation) / expSum;
                string className = classNames[i];
                Console.WriteLine($"{className}: {probability:P2}");
            }
            
            return 0;
        }
    }
}